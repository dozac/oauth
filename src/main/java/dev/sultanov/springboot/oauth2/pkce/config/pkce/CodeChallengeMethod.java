package dev.sultanov.springboot.oauth2.pkce.config.pkce;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.util.encoders.Hex;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public enum CodeChallengeMethod {
    S256 {
        @Override
        public String transform(String codeVerifier) {
            try {
//                MessageDigest digest = MessageDigest.getInstance("SHA-256");
//                byte[] hash = digest.digest(codeVerifier.getBytes(StandardCharsets.US_ASCII));
//                return Base64.getUrlEncoder().encodeToString(Hex.encode(hash));
                byte[] bytes = codeVerifier.getBytes("US-ASCII");
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                md.update(bytes, 0, bytes.length);
                byte[] digest = md.digest();
                return Base64.encodeBase64URLSafeString(digest);
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException e ) {
                throw new IllegalStateException(e);
            }

        }
    },
    PLAIN {
        @Override
        public String transform(String codeVerifier) {
            return codeVerifier;
        }
    },
    NONE {
        @Override
        public String transform(String codeVerifier) {
            throw new UnsupportedOperationException();
        }
    };

    public abstract String transform(String codeVerifier);
}
